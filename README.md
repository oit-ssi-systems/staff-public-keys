# Staff Public GPG Keys

Systems keys that once lived [here](https://wiki.duke.edu/display/oitssisys/Staff+GPG+public+key+listing)


## Systems GPG Quick Start

Original Wiki Article: https://wiki.duke.edu/display/oitssisys/Systems+GPG+Quick+Start

Gnu Privacy Guard (GPG or GnuPG) is an open source implementation of PGP. If you don't already have a GPG key, and you're unfamiliar with how to generate one, these instructions should be able to get you going at least as far as is necessary for putting your key up here and getting in on the encrypted password files.

### Installing GnuPG and Generating a Key Pair

1. First, download and install Gnu Privacy Guard:
    * Windows – [GnuPG](https://www.gnupg.org/ftp/gcrypt/binary) or `choco install gnupg`
    * Mac OS X – [GPGTools](https://gpgtools.org/) or [GPG for OSX](https://sourceforge.net/p/gpgosx/docu/Download/)
    * Linux – `yum install gnupg` or `apt-get install gnupg`
2. GPG is a command-line tool, so open up a terminal or console window. Generate a key pair – one private that you and only you possess and one public that you give to other people to sign stuff for you:
```
gpg --full-generate-key
```
3. When asked for the key type, take the default of RSA or if you have a newer gpg pick one of the newer Eliptical Curve options (ECC).
4. If you're using RSA a keysize of >2048 should be chosen.
5. Tell it to never expire the key – self-expiring keys are good in many cases, but we don't really have room here for both sides of the debate. Setting it to never expire will prevent you from losing access to old password files in case you need to read them.
6. When asked "Is this correct? (y/N)" answer y (unless, of course, it isn't correct).
7. For your real name, use whatever name you prefer.
8. The email address you give should be your preferred email address as listed in the online directory.
9. For the "Comment" option, you can specify anything you like (or nothing at all) – it's just a helper to assist you with keeping track of what the key is for, in case you have multiple keys with the same email address.
10. When prompted with "Change (N)ame, (C)omment, (E)mail or (O)kay/(Q)uit?" enter o.
11. You'll then be prompted for a passphrase. **This is important**! Your passphrase is what keeps your GPG key from being used if it fell into the wrong hands, like if the laptop on which it lived got stolen. It should be long and complicated because you don't need to type it very often, and if someone succeeded in guessing it or brute-force cracking it they could act as you with regard to signing or decrypting things, like all our important passwords. There is no way to recover a lost passphrase, either, so make sure you can either remember it or keep it written down somewhere very safe and with no identifying information to tell any finders what it might be. You'll need to type the passphrase twice to make sure you didn't mistype it, as well.
12. Once you've entered all that information, GPG will begin generating your keys. This might take a moment or three. When it's done, your GPG data will be saved in `~/.gnupg` (Linux and Mac OS X) or `%USERPROFILE%\AppData\Roaming\gnupg` (Windows).
13. Check to make sure your new keys are there and make a note of the GPG key ID which is in the **sec** field :
```
gpg --list-secret-keys --keyid-format LONG
```
14. You might want to make a backup copy of your .gnupg or gnupg directory to somewhere secure, in the event that the location in which it lives goes away (hard drives fail).

### Exporting Your Key for Loading Into Confluence

1. Export your public key (replace NETID with your NetID) and [GPG_KEY_ID] with the ID of your key from previous step:
```
gpg --export --armor [GPG_KEY_ID] > NETID.gpg
```
2. Upload this to the Staff GPG public key repository on gitlab.oit.duke.edu page, and add a line in the appropriate place in the listing with everyone's keys.

The next time someone generates a new password file, they'll download your public key, import it into their keyring, and sign it against that key so that you can then read it.

### Reading an Encrypted Password File

The password files uploaded to this page are just regular text files, encrypted with GPG against a bunch of people's public keys. Anyone whose public key is used in the encryption of the file can use their private key to decrypt it and read the contents.

1. Download the file you want and save it somewhere.
2. Run:
```
gpg --decrypt FILE
```
3. You'll be prompted for your passphrase, and you might see a bunch of mentions of unknown keys being used to sign, but down towards the bottom you should see the decrypted password displayed.

### Importing Other People's Public Keys

While not strictly necessary for using the encrypted password files, it is nice to have other people's keys in your keyring so that you don't see so many errors about unknown keys, and so you can send stuff encrypted only for them.

1. Download the key file you want from the Staff GPG public key listing and save it somewhere.
2. Run:
```
gpg --import FILE
```

### Adding GPG Public Key to GitLab

You can use this same GPG key to sign your commits in GitLab. See instructions here for more details: https://gitlab.oit.duke.edu/help/user/project/repository/gpg_signed_commits/index.md#adding-a-gpg-key-to-your-account
